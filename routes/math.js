var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  let result = [];
  for (var i = 0; i < 1000; i++) {
    result.push(i);
  }
  res.send({
    result
  })
});

module.exports = router;
